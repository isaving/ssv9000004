//Version: v0.0.1
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV9000004I struct {
	QryTpy        string `json:"QryTpy" validate:"required,max=30" description:"Query type"`     //查询类型  0-介质  1-合约  2-介质用途
	MediaType     string `json:"MediaType" validate:"max=4" description:"Medium type"`           //介质类型
	MediaNm       string `json:"MediaNm" validate:"max=40" description:"Medium number"`          //介质号码
	Currency      string `json:"Currency" validate:"max=3" description:"Currency code"`          //币种
	CashTranFlag  string `json:"CashTranFlag" validate:"max=3" description:"Cash transfer flag"` //钞汇标识
	ChannelNm     string `json:"ChannelNm" validate:"max=4" description:"Channel number"`        //渠道号
	UsgCod        string `json:"UsgCod" validate:"max=4" description:"Usage code"`               //用途代码
	AgreementId   string `json:"AgreementId" validate:"max=30" description:"Contract number"`    //合约号
	AgreementType string `json:"AgreementType" validate:"max=5" description:"Contract type"`     //合约类型
}

type SSV9000004O struct {
	RecordTotNum int
	Records []SSV9000004ORecord
}

type SSV9000004ORecord struct {
	AgreementID      string `json:"AgreementID" description:"Contract number"`              //合约号
	AgreementType    string `json:"AgreementType" description:"Contract type"`              //合约类型
	Currency         string `json:"Currency" description:"Currency"`                        //币种
	CashtranFlag     string `json:"CashtranFlag" description:"Flag of cash and remittance"` //钞汇标志
	AgreementStatus  string `json:"AgreementStatus" description:"Contract status"`          //合约状态
	AccOpnDt         string `json:"AccOpnDt" description:"Account opening date"`            //开户日期
	CstmrCntctPh     string `json:"CstmrCntctPh" description:"Mobile phone number"`         //手机号
	CstmrCntctAdd    string `json:"CstmrCntctAdd" description:"Customer contact address"`   //联系地址
	CstmrCntctEm     string `json:"CstmrCntctEm" description:"Customer email"`              //邮箱
	AccAcount        string `json:"AccAcount" description:"Accounting account number"`      //核算账号
	AccPsw           string `json:"AccPsw" description:"Account password"`                  //账户密码
	WdrwlMthd        string `json:"WdrwlMthd" description:"Withdrawal method"`              //支取方式
	CstmrId          string `json:"CstmrId" description:"Customer number"`                  //客户编号
	FreezeType       string `json:"FreezeType" description:"Frozen state"`                  //冻结状态
	DepcreFlag       string `json:"DepcreFlag" description:"Debit credit control mark"`     //借贷记控制标志
	AccuntNme        string `json:"AccuntNme" description:"Account name"`                   //账户姓名
	PrductId         string `json:"PrductId" description:"Product number"`                  //产品号
	PrductNm         int    `json:"PrductNm" description:"Product serial number"`           //产品顺序号
	DefaultAgreement string `json:"DefaultAgreement" description:"Default contract flag"`   //默认合约标识
}

// @Desc Build request message
func (o *SSV9000004I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV9000004I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV9000004O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV9000004O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV9000004I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV9000004I) GetServiceKey() string {
	return "ssv9000004"
}
