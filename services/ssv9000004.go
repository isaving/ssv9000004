//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/sv/ssv9000004/constant"
	"git.forms.io/isaving/sv/ssv9000004/dao"
	"git.forms.io/isaving/sv/ssv9000004/models"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/services"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/xormplus/xorm"
)

type Ssv9000004Impl struct {
	services.CommonService
	//TODO ADD Service Self Define Field
	Sv900004O *models.SSV9000004O
	Sv900004I *models.SSV9000004I
	//ContInfo  []dao.TPersonArgeement
	ContInfo  []models.SSV9000004ORecord
	O         *xorm.Engine
}

// @Desc Ssv9000004 process
// @Author
// @Date 2020-12-04
func (impl *Ssv9000004Impl) Ssv9000004(ssv9000004I *models.SSV9000004I) (ssv9000004O *models.SSV9000004O, err error) {

	impl.Sv900004I = ssv9000004I
	//impl.ContInfo = []dao.TPersonArgeement{}
	impl.ContInfo = []models.SSV9000004ORecord{}

	switch impl.Sv900004I.QryTpy {
	case "1":
		if impl.Sv900004I.AgreementId == "" || impl.Sv900004I.AgreementType == "" {
			return nil, errors.New("Agreement Id and Agreement Type can not be null", constant.ERRCODE03)
		}
		contInfo, err := dao.QueryContInfo(ssv9000004I.AgreementId, ssv9000004I.AgreementType,impl.O)
		if err != nil {
			return nil, err
		}
		log.Debug("contInfo:", contInfo)
		if contInfo == nil {
			return nil, errors.New("Record not found of query contract", constant.ERRCODE04)
		}
		record := models.SSV9000004ORecord{
			AgreementID:      contInfo.AgreementId,
			AgreementType:    contInfo.AgreementType,
			Currency:         contInfo.Currency,
			CashtranFlag:     contInfo.CashtranFlag,
			AgreementStatus:  contInfo.AgreementStatus,
			AccOpnDt:         contInfo.AccOpnDt,
			CstmrCntctPh:     contInfo.CstmrCntctPh,
			CstmrCntctAdd:    contInfo.CstmrCntctAdd,
			CstmrCntctEm:     contInfo.CstmrCntctEm,
			AccAcount:        contInfo.AccAcount,
			AccPsw:           contInfo.AccPsw,
			WdrwlMthd:        contInfo.WdrwlMthd,
			CstmrId:          contInfo.CstmrId,
			FreezeType:       contInfo.FreezeType,
			DepcreFlag:       contInfo.DepcreFlag,
			AccuntNme:        contInfo.AccuntNme,
			PrductId:         contInfo.PrductId,
			PrductNm:         contInfo.PrductNm,
			DefaultAgreement: "",
		}
		impl.ContInfo = append(impl.ContInfo,record)

		//impl.ContInfo = append(impl.ContInfo, *contInfo)

	case "0":
		if impl.Sv900004I.MediaNm == "" || impl.Sv900004I.MediaType == "" {
			return nil, errors.New("Media number Id and Media Type can not be null", constant.ERRCODE02)
		}
		relInfo, err := dao.QueryContMediumRel(ssv9000004I.MediaNm, ssv9000004I.MediaType,impl.O)
		if err != nil {
			return nil,err
		}
		if len(relInfo) == 0 {
			return nil, errors.New("Record not found by Media number", constant.ERRCODE04)
		}

		//第一次循环，取默认合约标识=Y 的记录
		if len(relInfo) > 0 {
			for _, data := range relInfo {
				if data.DefaultAgreement == "Y" {
					contInfo, err := dao.QueryContInfo(data.AgreementId, data.AgreementType,impl.O)
					if err != nil{
						return nil,err
					}
					log.Debug("Query contract result",contInfo)
					if contInfo == nil {
						return nil, errors.New("Record not found of query contract", constant.ERRCODE04)
					}
					record := models.SSV9000004ORecord{
						AgreementID:      contInfo.AgreementId,
						AgreementType:    contInfo.AgreementType,
						Currency:         contInfo.Currency,
						CashtranFlag:     contInfo.CashtranFlag,
						AgreementStatus:  contInfo.AgreementStatus,
						AccOpnDt:         contInfo.AccOpnDt,
						CstmrCntctPh:     contInfo.CstmrCntctPh,
						CstmrCntctAdd:    contInfo.CstmrCntctAdd,
						CstmrCntctEm:     contInfo.CstmrCntctEm,
						AccAcount:        contInfo.AccAcount,
						AccPsw:           contInfo.AccPsw,
						WdrwlMthd:        contInfo.WdrwlMthd,
						CstmrId:          contInfo.CstmrId,
						FreezeType:       contInfo.FreezeType,
						DepcreFlag:       contInfo.DepcreFlag,
						AccuntNme:        contInfo.AccuntNme,
						PrductId:         contInfo.PrductId,
						PrductNm:         contInfo.PrductNm,
						DefaultAgreement: data.DefaultAgreement,
					}
					impl.ContInfo = append(impl.ContInfo,record)
				}
			}
		}

		//第二次循环，取默认合约标识不等于Y 的记录
		if len(relInfo) > 0 {
			for _, data := range relInfo {
				if data.DefaultAgreement != "Y" {
					contInfo, err := dao.QueryContInfo(data.AgreementId, data.AgreementType,impl.O)
					if err != nil{
						return nil,err
					}
					log.Debug("Query contract result",contInfo)
					if contInfo == nil {
						return nil, errors.New("Record not found of query contract", constant.ERRCODE04)
					}
					record := models.SSV9000004ORecord{
						AgreementID:      contInfo.AgreementId,
						AgreementType:    contInfo.AgreementType,
						Currency:         contInfo.Currency,
						CashtranFlag:     contInfo.CashtranFlag,
						AgreementStatus:  contInfo.AgreementStatus,
						AccOpnDt:         contInfo.AccOpnDt,
						CstmrCntctPh:     contInfo.CstmrCntctPh,
						CstmrCntctAdd:    contInfo.CstmrCntctAdd,
						CstmrCntctEm:     contInfo.CstmrCntctEm,
						AccAcount:        contInfo.AccAcount,
						AccPsw:           contInfo.AccPsw,
						WdrwlMthd:        contInfo.WdrwlMthd,
						CstmrId:          contInfo.CstmrId,
						FreezeType:       contInfo.FreezeType,
						DepcreFlag:       contInfo.DepcreFlag,
						AccuntNme:        contInfo.AccuntNme,
						PrductId:         contInfo.PrductId,
						PrductNm:         contInfo.PrductNm,
						DefaultAgreement: data.DefaultAgreement,
					}
					impl.ContInfo = append(impl.ContInfo,record)
				}
			}
		}

	case "2":
		//校验
		if impl.Sv900004I.MediaNm == "" || impl.Sv900004I.MediaType == "" {
			return nil, errors.New("Media number Id and Media Type can not be null", constant.ERRCODE02)
		}
		if impl.Sv900004I.Currency == "" {
			return nil, errors.New("Currency code can not be null", constant.ERRCODE05)
		}
		if impl.Sv900004I.CashTranFlag == "" {
			return nil, errors.New("Cash transfer flag can not be null", constant.ERRCODE06)
		}
		if impl.Sv900004I.ChannelNm == "" {
			return nil, errors.New("Channel number can not be null", constant.ERRCODE07)
		}
		if impl.Sv900004I.UsgCod == "" {
			return nil, errors.New("Usage code can not be null", constant.ERRCODE08)
		}

		Media := &dao.TMdslagp{
			MdsType:          impl.Sv900004I.MediaType,
			MdsNm:            impl.Sv900004I.MediaNm,
			Currency:         impl.Sv900004I.Currency,
			CashtranFlag:     impl.Sv900004I.CashTranFlag,
			ChannelNm:        impl.Sv900004I.ChannelNm,
			UsgCod:           impl.Sv900004I.UsgCod,
		}

		//查询介质与合约关系表
		contNum := ""
		contType := ""
		data,err :=dao.QueryContMediumRelByStruct(Media,impl.O)
		if err != nil {
			log.Debug("0")
			return nil,err
		}
		if data == nil {
			//获取默认合约标识=Y的记录
			queryData := &dao.TMdslagp{
				MdsType:          impl.Sv900004I.MediaType,
				MdsNm:            impl.Sv900004I.MediaNm,
				DefaultAgreement: "Y",

			}
			data1,err :=dao.QueryContMediumRelByStruct(queryData,impl.O)
			if err != nil{
				log.Debug("1")
				return nil,err
			}
			if data1 == nil {
				return nil, errors.New("Record not found by Media number", constant.ERRCODE04)
			}
			contNum = data1.AgreementId
			contType = data1.AgreementType

		}else {
			contNum = data.AgreementId
			contType = data.AgreementType
		}

		//查询合约表
		contInfor, err := dao.QueryContInfo(contNum,contType,impl.O)
		if err != nil {
			log.Debug("2")
			return nil,err
		}
		if contInfor == nil {
			return nil, errors.New("Record not found of query contract", constant.ERRCODE04)
		}
		log.Debug("contInfor:",contInfor)
		record := models.SSV9000004ORecord{
			AgreementID:      contInfor.AgreementId,
			AgreementType:    contInfor.AgreementType,
			Currency:         contInfor.Currency,
			CashtranFlag:     contInfor.CashtranFlag,
			AgreementStatus:  contInfor.AgreementStatus,
			AccOpnDt:         contInfor.AccOpnDt,
			CstmrCntctPh:     contInfor.CstmrCntctPh,
			CstmrCntctAdd:    contInfor.CstmrCntctAdd,
			CstmrCntctEm:     contInfor.CstmrCntctEm,
			AccAcount:        contInfor.AccAcount,
			AccPsw:           contInfor.AccPsw,
			WdrwlMthd:        contInfor.WdrwlMthd,
			CstmrId:          contInfor.CstmrId,
			FreezeType:       contInfor.FreezeType,
			DepcreFlag:       contInfor.DepcreFlag,
			AccuntNme:        contInfor.AccuntNme,
			PrductId:         contInfor.PrductId,
			PrductNm:         contInfor.PrductNm,
			DefaultAgreement: "Y",
		}
		impl.ContInfo = append(impl.ContInfo,record)
	default:

		return nil, errors.New("Query type is error", constant.ERRCODE01)
	}

	ssv9000004O = &models.SSV9000004O{
		RecordTotNum: len(impl.ContInfo),
		Records:      impl.ContInfo,
	}

	return ssv9000004O, nil
}
