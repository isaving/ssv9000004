package services

import (
	"git.forms.io/isaving/sv/ssv9000004/dao"
	"git.forms.io/isaving/sv/ssv9000004/models"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/astaxie/beego"
	. "github.com/smartystreets/goconvey/convey"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	_ = beego.LoadAppConfig("ini", "../conf/app.conf")
	_ = dao.InitDatabase()
	exitVal := m.Run()
	os.Exit(exitVal)
}

func TestEul20001Impl_Eul20001_Success(t *testing.T) {
	input := &models.SSV9000004I{
		QryTpy:        "0",
		MediaType:     "1",
		MediaNm:       "1",
		AgreementId:   "",
		AgreementType: "",
	}
	input1 := &models.SSV9000004I{
		QryTpy:        "1",
		MediaType:     "",
		MediaNm:       "",
		AgreementId:   "1",
		AgreementType: "1",
	}
	input2 := &models.SSV9000004I{
		QryTpy:        "3",
		MediaType:     "",
		MediaNm:       "",
		AgreementId:   "1",
		AgreementType: "1",
	}
	input3 := &models.SSV9000004I{
		QryTpy:        "0",
		MediaType:     "",
		MediaNm:       "",
		AgreementId:   "",
		AgreementType: "",
	}
	input4 := &models.SSV9000004I{
		QryTpy:        "1",
		MediaType:     "",
		MediaNm:       "",
		AgreementId:   "",
		AgreementType: "",
	}

	input5 := &models.SSV9000004I{
		QryTpy:        "",
		MediaType:     "",
		MediaNm:       "",
		AgreementId:   "",
		AgreementType: "",
	}

	impl := Ssv9000004Impl{
		Sv900004I: input,
		O :  dao.EngineCache,
	}

	Convey("Call service", t, func() {
		//var session *dao.Session
		Convey("Mock data", func() {
			db, mock, err := sqlmock.New()
			So(err, ShouldBeNil)
			dao.EngineCache.DB().DB = db
			mock.ExpectQuery("t_person_argeement").
				WillReturnRows(sqlmock.NewRows([]string{"mds_nm", "mds_type"}).AddRow("1", "1"))
			mock.ExpectQuery("t_mdslagp").
				WillReturnRows(sqlmock.NewRows([]string{"agreement_id", "agreement_type"}).AddRow("1", "1"))
			Convey("Validate session", func() {
				_, _ = impl.Ssv9000004(input)
				_, _ = impl.Ssv9000004(input1)
				_, _ = impl.Ssv9000004(input2)
				_, _ = impl.Ssv9000004(input3)
				_, _ = impl.Ssv9000004(input4)
				_, _ = impl.Ssv9000004(input5)
			})
		})

	})

	Convey("Call service", t, func() {
		//var session *dao.Session
		Convey("Mock data", func() {
			db, mock, err := sqlmock.New()
			So(err, ShouldBeNil)
			dao.EngineCache.DB().DB = db
			mock.ExpectQuery("t_person_argeement").
				WillReturnRows(sqlmock.NewRows([]string{"mds_nm", "mds_type"}).RowError(1, err))
			mock.ExpectQuery("t_mdslagp").
				WillReturnRows(sqlmock.NewRows([]string{"agreement_id", "agreement_type"}).AddRow("1", "1"))
			Convey("Validate session", func() {
				_, _ = impl.Ssv9000004(input)
				_, _ = impl.Ssv9000004(input1)
				_, _ = impl.Ssv9000004(input2)
				_, _ = impl.Ssv9000004(input3)
				_, _ = impl.Ssv9000004(input4)
				_, _ = impl.Ssv9000004(input5)
			})
		})

	})

	Convey("Call service", t, func() {
		//var session *dao.Session
		Convey("Mock data", func() {
			db, mock, err := sqlmock.New()
			So(err, ShouldBeNil)
			dao.EngineCache.DB().DB = db
			mock.ExpectQuery("t_person_argeement").
				WillReturnRows(sqlmock.NewRows([]string{"mds_nm", "mds_type"}).AddRow("1", "1"))
			mock.ExpectQuery("t_mdslagp").
				WillReturnRows(sqlmock.NewRows([]string{"agreement_id", "agreement_type"}).RowError(1, err))
			Convey("Validate session", func() {
				_, _ = impl.Ssv9000004(input)
				_, _ = impl.Ssv9000004(input1)
				_, _ = impl.Ssv9000004(input2)
				_, _ = impl.Ssv9000004(input3)
				_, _ = impl.Ssv9000004(input4)
				_, _ = impl.Ssv9000004(input5)
			})
		})

	})

	Convey("Call service", t, func() {
		//var session *dao.Session
		Convey("Mock data", func() {
			db, mock, err := sqlmock.New()
			So(err, ShouldBeNil)
			dao.EngineCache.DB().DB = db
			mock.ExpectQuery("t_mdslagp").
				WillReturnRows(sqlmock.NewRows([]string{"agreement_id", "agreement_type"}).AddRow("1","1"))
			mock.ExpectQuery("t_person_argeement").
				WillReturnRows(sqlmock.NewRows([]string{"mds_nm", "mds_type"}).AddRow("1", "1"))
			Convey("Validate session", func() {
				_, _ = impl.Ssv9000004(input)
				_, _ = impl.Ssv9000004(input1)
				_, _ = impl.Ssv9000004(input2)
				_, _ = impl.Ssv9000004(input3)
				_, _ = impl.Ssv9000004(input4)
				_, _ = impl.Ssv9000004(input5)
			})
		})

	})

}
