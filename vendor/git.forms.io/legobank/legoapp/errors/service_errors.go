package errors

import (
	"bytes"
	"fmt"
	"reflect"
	"runtime"
)

type ServiceError struct {
	ErrorCode string
	ErrorArgs []interface{}
	Err       error
	stack     []uintptr
	frames    []StackFrame
	prefix    string
}

// The maximum number of stackframes on any error.
var MaxStackDepth = 50

// New makes an Error from the given value. If that value is already an
// error then it will be used directly, if not, it will be passed to
// fmt.Errorf("%v"). The stacktrace will point to the line of code that
// called New.
func New(e interface{}, errorCode string) *ServiceError {
	var err error

	switch e := e.(type) {
	case error:
		err = e
	default:
		err = fmt.Errorf("%v", e)
	}

	stack := make([]uintptr, MaxStackDepth)
	length := runtime.Callers(2, stack[:])
	return &ServiceError{
		ErrorCode: errorCode,
		Err:       err,
		stack:     stack[:length],
	}
}

// Wrap makes an Error from the given value. If that value is already an
// error then it will be used directly, if not, it will be passed to
// fmt.Errorf("%v"). The skip parameter indicates how far up the stack
// to start the stacktrace. 0 is from the current call, 1 from its caller, etc.
func Wrap(e interface{}, skip int, errorCode string, args ...interface{}) *ServiceError {
	//if e == nil {
	//	return nil
	//}

	var err error

	switch e := e.(type) {
	case *ServiceError:
		return e
	case error:
		err = e
	default:
		err = fmt.Errorf("%v", e)
	}

	stack := make([]uintptr, MaxStackDepth)
	length := runtime.Callers(2+skip, stack[:])
	return &ServiceError{
		ErrorCode: errorCode,
		ErrorArgs: args,
		Err:       err,
		stack:     stack[:length],
	}
}

// WrapPrefix makes an Error from the given value. If that value is already an
// error then it will be used directly, if not, it will be passed to
// fmt.Errorf("%v"). The prefix parameter is used to add a prefix to the
// error message when calling Error(). The skip parameter indicates how far
// up the stack to start the stacktrace. 0 is from the current call,
// 1 from its caller, etc.
func WrapPrefix(e interface{}, prefix string, skip int, errorCode string, args ...interface{}) *ServiceError {
	//if e == nil {
	//	return nil
	//}

	err := Wrap(e, 1+skip, errorCode)

	if err.prefix != "" {
		prefix = fmt.Sprintf("%s: %s", prefix, err.prefix)
	}

	return &ServiceError{
		Err:    err.Err,
		stack:  err.stack,
		prefix: prefix,
	}

}

// Is detects whether the error is equal to a given error. Errors
// are considered equal by this function if they are the same object,
// or if they both contain the same error inside an errors.Error.
func Is(e error, original error) bool {

	if e == original {
		return true
	}

	if e, ok := e.(*ServiceError); ok {
		return Is(e.Err, original)
	}

	if original, ok := original.(*ServiceError); ok {
		return Is(e, original.Err)
	}

	return false
}

// Errorf creates a new error with the given message. You can use it
// as a drop-in replacement for fmt.Errorf() to provide descriptive
// errors in return values.
func Errorf(errorCode string, format string, a ...interface{}) *ServiceError {
	return Wrap(fmt.Errorf(format, a...), 1, errorCode, a...)
}

// Error returns the underlying error's message.
func (err *ServiceError) Error() string {

	msg := err.Err.Error()
	if err.prefix != "" {
		msg = fmt.Sprintf("%s: %s", err.prefix, msg)
	}

	return msg
}

// Stack returns the callstack formatted the same way that go does
// in runtime/debug.Stack()
func (err *ServiceError) Stack() []byte {
	buf := bytes.Buffer{}

	for _, frame := range err.StackFrames() {
		buf.WriteString(frame.String())
	}

	return buf.Bytes()
}

// Callers satisfies the bugsnag ErrorWithCallerS() interface
// so that the stack can be read out.
func (err *ServiceError) Callers() []uintptr {
	return err.stack
}

// ErrorStack returns a string that contains both the
// error message and the callstack.
func (err *ServiceError) ErrorStack() string {
	return err.TypeName() + " " + err.Error() + "\n" + string(err.Stack())
}

// StackFrames returns an array of frames containing information about the
// stack.
func (err *ServiceError) StackFrames() []StackFrame {
	if err.frames == nil {
		err.frames = make([]StackFrame, len(err.stack))

		for i, pc := range err.stack {
			err.frames[i] = NewStackFrame(pc)
		}
	}

	return err.frames
}

// TypeName returns the type this error. e.g. *errors.stringError.
func (err *ServiceError) TypeName() string {
	if _, ok := err.Err.(uncaughtPanic); ok {
		return "panic"
	}
	return reflect.TypeOf(err.Err).String()
}

func GetErrorCode(err error) string {

	if err == nil {
		return ""
	}

	switch err := err.(type) {
	case *ServiceError:
		return err.ErrorCode
	default:
		return ""
	}

}

func ServiceErrorToString(err error) string {
	var str string
	if err != nil {
		switch err.(type) {
		case *ServiceError:
			if nil != err.(*ServiceError) && nil != err.(*ServiceError).Err {
				str = err.(*ServiceError).ErrorStack()
			} else {
				var buf [2 << 10]byte
				stackFrame := string(buf[:runtime.Stack(buf[:], true)])
				str = fmt.Sprintf("Input Error instance invalid, stack frame=%s, please check,err=%++v", stackFrame, err)
			}
		default:
			str = err.Error()
		}
		//} else {
		//	var buf [2 << 10]byte
		//	stackFrame := string(buf[:runtime.Stack(buf[:], true)])
		//	str = fmt.Sprintf("Input Error instance is nil, stack frame=%s, please check!", stackFrame)
	}

	return str
}
