package util

import (
	"git.forms.io/legobank/legoapp/constant"
	"time"
)

// @Desc Check date format
func ValidDate(timeStr string) error {
	_, err := time.Parse("2006-01-02", timeStr)
	return err
}

// @Desc Get first day of month
func GetFirstDateOfMonth() string {
	d := time.Now()
	timestamp := time.Now().AddDate(0, 0, -d.Day()+1).Unix()
	tm := time.Unix(timestamp, 0)
	return tm.Format("2006-01-02")
}

// @Desc  get current time ,fmt: hh:mm:ss
func GetCurrentTimeFmt() string {
	return time.Now().Format("15:04:05")
}

// @Desc  fmt timestamp ,fmt:"yyyy-mm-dd hh:mm:ss"
func GetTimeFormat(timestmp int64) string {
	t := time.Unix(timestmp, 0).Format(constant.TIME_STAMP)
	return t
}

// @Desc Get timestamp
func GetTimeParse(timestr string) (time.Time, error) {
	t, err := time.ParseInLocation(constant.TIME_STAMP, timestr, time.Local)
	return t, err
}

// @Desc get current time
func GetCurTime() time.Time {
	t := time.Now()
	return t
}

// @Desc get current date&& time ,fmt:yyyy-mm-dd,hh:mm:ss
func GetCurrentTimeStamp() (string, string) {
	timestamp := time.Now()
	return timestamp.Format("2006-01-02"), timestamp.Format("15:04:05")
}

// @Desc Get current datetime
func GetCurrentDateTime() string {
	return time.Now().Format("2006-01-02 15:04:05")
}

// @Desc Get current date
func GetCurrentDate() string {
	return time.Now().Format("2006-01-02")
}

func GetCurrentTime() string {
	return time.Now().Format("15:04:05")
}

// @Desc Get the date of months ago
func GetPreMonthDate(monthNum int) string {
	now := time.Now()
	yesterday := now.AddDate(0, -monthNum, 0)
	return yesterday.Format("2006-01-02")
}

// @Desc Get current datetime of Thailand
func GetTHCurrentDateTime() string {
	var cstZone = time.FixedZone("CST", 7*3600)
	return time.Now().In(cstZone).Format("2006-01-02 15:04:05")
}
