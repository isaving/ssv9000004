package dao

import (
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/xormplus/xorm"
)

type TMdslagp struct {
	MediaAgreementId string    `orm:"column(media_agreement_id);size(20);pk" description:"介质和合约关系ID"`
	MdsType          string    `orm:"column(mds_type);size(3)" description:"介质类型"`
	MdsNm            string    `orm:"column(mds_nm);size(60)" description:"介质号码"`
	Currency         string    `orm:"column(currency);size(3);null" description:"币别"`
	CashtranFlag     string    `orm:"column(cashtran_flag);size(1);null" description:"钞汇标志"`
	ChannelNm        string    `orm:"column(channel_nm);size(4);null" description:"渠道号"`
	UsgCod           string    `orm:"column(usg_cod);size(3);null" description:"用途代码"`
	AgreementId      string    `orm:"column(agreement_id);size(30)" description:"合约号"`
	AgreementType    string    `orm:"column(agreement_type);size(5);null" description:"合约类型"`
	DefaultAgreement string    `orm:"column(default_agreement);size(1);" description:"默认合约标识"`
	EffectiveFlag    string    `orm:"column(effective_flag);size(1);null" description:"有效标志"`
	LastUpDatetime   string    `orm:"column(last_up_datetime);type(datetime)" description:"最后更新日期时间"`
	LastUpBn         string    `orm:"column(last_up_bn);size(30);null" description:"最后更新行所"`
	LastUpEm         string    `orm:"column(last_up_em);size(30);null" description:"最后更新柜员"`
}

func (t *TMdslagp) TableName() string {
	return "t_mdslagp"
}

//查询介质与合约关系表
func QueryContMediumRel(mediaNm string, mediaType string,O *xorm.Engine) ([]TMdslagp, error) {

	relCont := []TMdslagp{}
	err := O.Where("mds_nm = ? and mds_type = ?", mediaNm, mediaType).Find(&relCont)
	if err != nil {
		return nil, err
	}

	return relCont, nil
}

//查询介质与合约关系表（不指定查询条件）
func QueryContMediumRelByStruct(data *TMdslagp,O *xorm.Engine)(result *TMdslagp,err error) {

	has,err :=O.Get(data)
	if err != nil  {
		log.Error(err)
		return nil,err
	}
	if !has {
		return nil,nil
	}

	return data,nil
}