package dao

import (
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/xormplus/xorm"
)

type TPersonArgeement struct {
	AgreementId     string  `orm:"column(agreement_id);pk" description:"合约号8+12+1 8位银行号：乐高默认是62822222 12位编码：顺序号 1位校验位 "`
	AgreementType   string  `orm:"column(agreement_type);size(5)" description:"合约类型"`
	Currency        string  `orm:"column(currency);size(3)" description:"币种			"`
	CashtranFlag    string  `orm:"column(cashtran_Flag);size(1)" description:"钞汇标识 C-钞户;T-汇户"`
	AgreementStatus string  `orm:"column(agreement_status);size(1)" description:"合约状态 0-正常;1-销户计息;2-结清;3-解约"`
	FreezeType      string  `orm:"column(freeze_type);size(1)" description:"冻结状态 0-正常;1-合约冻结;2-金额冻结;3-暂禁"`
	TermFlag        string  `orm:"column(term_flag);size(1)" description:"是否分期标志 1-不分期；2-分期"`
	DepcreFlag      string  `orm:"column(depcre_flag);size(1)" description:"借贷记控制标志 N-正常;D-只借记;C-只贷记;F-不允许借贷记"`
	AccFlag         string  `orm:"column(acc_flag);size(1)" description:"是否核算标识  0-是;1-否;"`
	AccAcount       string  `orm:"column(acc_acount);size(32);null" description:"核算账号"`
	BegnTxDt        string  `orm:"column(begn_tx_dt);type(date);null" description:"起息日期"`
	AppntDtFlg      string  `orm:"column(appnt_dt_flg);type(date);null" description:"是否指定到期日0-是;1-否;2-未知"`
	AppntDt         string  `orm:"column(appnt_dt);type(date);null" description:"到期日期"`
	PrductId        string  `orm:"column(prduct_id);size(30)" description:"产品号"`
	PrductNm        int     `orm:"column(prduct_nm)" description:"产品顺序号"`
	CstmrId         string  `orm:"column(cstmr_id);size(20)" description:"客户编号"`
	CstmrTyp        string  `orm:"column(cstmr_typ);size(2)" description:"客户类型1-个人客户;2-法人客户;3-联名客户;4-集团客户;5-客户关系个人信息;6-客户关系对公信息;7-内部户客户;8-集团成员客户"`
	AccuntNme       string  `orm:"column(accunt_nme);size(120);null" description:"账户名称"`
	UsgCod          string  `orm:"column(usg_cod);size(12);null" description:"用途代码"`
	WdrwlMthd       string  `orm:"column(wdrwl_mthd);size(1)" description:"支取方式0-密码;1-印鉴;2-签字、3-指纹;4-密码+印鉴;5-密码+签字;6-密码+指纹;7-印鉴+签字;8-印鉴+指纹;9-签字+指纹"`
	AccPsw          string  `orm:"column(acc_psw);size(16);null" description:"账户密码"`
	PswWrongTime    int     `orm:"column(psw_wrong_time);null" description:"密码错误次数"`
	TrnWdrwmThd     string  `orm:"column(trn_wdrwm_thd);size(1);null" description:"通兑方式 0-非通存通兑;1-分行通兑;2-全行通兑"`
	OpnAmt          float64 `orm:"column(opn_amt);null;digits(18);decimals(2)" description:"开户金额            "`
	AccCnclFlg      string  `orm:"column(acc_cncl_flg);size(1);null" description:"销户启用标识 Y-启用;N-不启用"`
	AutCnl          string  `orm:"column(aut_cnl);size(1)" description:"是否允许自动销户 0-是;1-否;"`
	DytoCncl        int     `orm:"column(dyto_cncl);null" description:"自动销户宽限天数"`
	SttmntFlg       string  `orm:"column(sttmnt_flg);size(1)" description:"对账单标识 Y-出对账单;N-不出对账单"`
	WthdrwlMthd     string  `orm:"column(wthdrwl_mthd);size(1);null" description:"取款方式 1-存折;2-密码;3-证件;4-印鉴"`
	OvrDrwFlg       string  `orm:"column(ovr_drw_flg);size(1);null" description:"透支标识 N-不可透支;Y-可透支除金额冻结"`
	CstmrCntctAdd   string  `orm:"column(cstmr_cntct_add);size(200);null" description:"客户联系地址"`
	CstmrCntctPh    string  `orm:"column(cstmr_cntct_ph);size(20);null" description:"客户联系电话"`
	CstmrCntctEm    string  `orm:"column(cstmr_cntct_Em);size(200);null" description:"邮箱地址"`
	BnCrtAcc        string  `orm:"column(bn_crt_acc);size(30);null" description:"开户行所"`
	BnAcc           string  `orm:"column(bn_acc);size(30);null" description:"账务行所"`
	AccOpnDt        string  `orm:"column(acc_opn_dt);type(date)" description:"开户日期"`
	AccOpnEm        string  `orm:"column(acc_opn_em);size(30);null" description:"开户柜员"`
	AccCanclDt      string  `orm:"column(acc_cancl_dt);type(date);null" description:"销户日期"`
	AccCanclEm      string  `orm:"column(acc_cancl_em);size(30);null" description:"销户柜员"`
	AccCanclResn    string  `orm:"column(acc_cancl_resn);size(60);null" description:"销户原因"`
	LastUpDatetime  string  `orm:"column(last_up_datetime);type(datetime);null" description:"最后更新日期"`
	LastUpBn        string  `orm:"column(last_up_bn);size(30);null" description:"最后更新行所"`
	LastUpEm        string  `orm:"column(last_up_em);size(30);null" description:"最后更新柜员"`
	TccStatus       string  `orm:"column(tcc_status);size(1);null"`
}

func (t *TPersonArgeement) TableName() string {
	return "t_person_argeement"
}

//通过合约号查询合约信息表
func QueryContInfo(contNo string, contType string,O *xorm.Engine) (*TPersonArgeement, error) {

	personInfo := new(TPersonArgeement)
	flag, err := O.Where("agreement_id = ?", contNo).Where("agreement_type=?", contType).Get(personInfo)
	if err != nil {
		log.Error(err)
		return nil,err
	}
	if !flag {
		return nil, nil
	}

	return personInfo, nil
}