//Version: v0.0.1
package controllers

import (
	"git.forms.io/isaving/sv/ssv9000004/dao"
	"git.forms.io/isaving/sv/ssv9000004/models"
	"git.forms.io/isaving/sv/ssv9000004/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv9000004Controller struct {
	controllers.CommController
}

func (*Ssv9000004Controller) ControllerName() string {
	return "Ssv9000004Controller"
}

// @Desc ssv9000004 controller
// @Description Entry
// @Param ssv9000004 body models.SSV9000004I true "body for user content"
// @Success 200 {object} models.SSV9000004O
// @router /ssv9000004 [post]
// @Author
// @Date 2020-12-04
func (c *Ssv9000004Controller) Ssv9000004() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv9000004Controller.Ssv9000004 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv9000004I := &models.SSV9000004I{}
	if err := models.UnPackRequest(c.Req.Body, ssv9000004I); err != nil {
		c.SetServiceError(err)
		return
	}
	if err := ssv9000004I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv9000004 := &services.Ssv9000004Impl{}
	ssv9000004.New(c.CommController)
	ssv9000004.Sv900004I = ssv9000004I
	ssv9000004.O = dao.EngineCache

	ssv9000004O, err := ssv9000004.Ssv9000004(ssv9000004I)

	if err != nil {
		log.Errorf("Ssv9000004Controller.Ssv9000004 failed, err=%v", errors.ServiceErrorToString(err))
		c.SetServiceError(err)
		return
	}
	responseBody, err := ssv9000004O.PackResponse()
	if err != nil {
		c.SetServiceError(err)
		return
	}
	c.SetAppBody(responseBody)
}

// Title Ssv9000004 Controller
// Description ssv9000004 controller
// Param Ssv9000004 body models.SSV9000004I true body for SSV9000004 content
// Success 200 {object} models.SSV9000004O
// router /create [post]
func (c *Ssv9000004Controller) SWSsv9000004() {
	//Here is to generate API documentation, no need to implement methods
}
